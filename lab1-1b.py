from rozklady import geometryczny

p=0.75
n=100
print "Dla",n,"prob rozkladu geometrycznego z parametrem p=",p

losowyGeo=geometryczny.rozkladGeometryczny(p,n)
EX=losowyGeo.wartosc_oczekiwana(p)
DX=losowyGeo.wariancja(p)          
print losowyGeo.x,losowyGeo.y
print "podczas 1 proby jako pierwsze zwyciestwo osiagnelo",losowyGeo.y[1],"prob"
print "srednia=",losowyGeo.EX," oraz wariancja populacji=",losowyGeo.DX
print "wartosc oczekiwana=",EX, "oraz wariancja=",DX," dla parametrow n i p"


from pylab import *
xlabel('EX='+str(round(EX,2))+", DX="+str(round(DX,2))+", srednia="+str(round(losowyGeo.EX,2))+", war_pop="+str(round(losowyGeo.DX,2)))
ylabel('Ilosc zwyciestw przy x probach')
title('Wykres')
plot(losowyGeo.x, losowyGeo.y)
plot([losowyGeo.EX,losowyGeo.EX],[0,max(losowyGeo.y)])
plot([EX,EX],[0,max(losowyGeo.y)])
xlim(1)#minimum i max
savefig('lab1-1b.jpeg')
