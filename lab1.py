'''
Created on 15 paz 2015
@author: corcallosum
'''

"""
Zbadac eksperymentalnie i porownac (graficznie) z wynikami 
teoretycznymi koncentracje zmiennych losowych z wykladu:
a)    n prob Bernoulliego
b)    zmienna o rozkladzie geometrycznym
c)    kolekcjonowanie kuponow
d)    rzut kostka do uzyskania dwoch szostek pod rzad


Rozwazmy proces, w ktorym dane przychodza w trybie ciaglym, np. 
co sekunde. W kazdej paczce jest kilkadziesiat - kilkaset liczb.
 W kazdym momencie interesuja nas statystyki za ostatnia godzine.
a)    Oprogramowac powolne liczenie sredniej, wariancji i mediany i 
    innych statystyk pozycyjnych
b)    Oprogramowac szybkie liczenie sredniej i wariancji
c)    * Oprogramowac szybkie liczenie mediany i innych statystyk 
    pozycyjnych
"""
from math import factorial as factorial, sqrt
from pylab import *
from random import random,randrange
from scipy.special import binom

class rozkladDyskretny:
    def __init__(self,p=0.5,x=list(),y=list()):
        #prawdopodobienstwo
        self.p=p
        #wykres pkt os x
        self.x=x
        #wykres pkt os y
        self.y=y
        self.EX=self.oblicz_moment(1)
        #EX^2    
        self.EX2=self.oblicz_moment(2)
        #wariancja
        self.DX= self.oblicz_wariancje(self.EX, x, y)
    def oblicz_moment(self,momentx):
        #oblicz moment rzedu x
        moment=0
        n=len(self.x)
        for i in range(1,n):
            moment+=float(self.y[i])*float(i**(momentx))
        return moment
    def oblicz_wariancje(self,EX,x,y):
        wariancja=0
        n=sum(x)
        for i in range(1,lwaen(x)):
            wariancja+=float(x[i])*float((y[i]-EX)**2)
            
        wariancja/=n
        print "warrr",wariancja,EX
        return wariancja
    def generuj_probe_losowa(self,p):
        pass
    def generuj_probe_teoretyczna(self,p):
        pass   

class rozkladGeometryczny(rozkladDyskretny):
    #rozklad geometryczny  dyskretny rozklad prawdopodobienstwa 
    #opisujacy prawdopodobienstwo zdarzenia, ze proces Bernoulliego odniesie 
    #pierwszy sukces dokladnie w k tej probie    
    def __init__(self,p=0.5,n=100,losowa=1):
        #p-prawdopodobienstwo,n-ilosc prob, jesli klasa ma byc losowa=1
        self.n=n#ilosc prob
        if losowa==1:
            #zwraca tablice x oraz w ktorej probie najpozniejszy sukces
            z=list()
            z=self.generuj_probe_losowa(p) 
            x=z[0]
            y=range(z[1]+1)
        else:
            z=list()
            z=self.generuj_probe_teoretyczna(p)    
            y=z[1]
            x=z[0]#sprawdza jeden do przodu nie trzeba dodawac+1
        rozkladDyskretny.__init__(self, p, x, y)
    def generuj_probe_losowa(self,p):
        #zapelnianie wedlug wygenerowanych liczb
        max_sukces=0#nawieksza liczba proby dla ktorej wylosowano zwyciestwo
        y=dict()        
        for i in range(self.n):
            #przeprowadzamy n prob
            sukces=0
            ilosc_porazek=0
            while(sukces==0):
                probka=random()
                if(probka<p):
                    sukces=1
                    max_sukces=max(max_sukces,ilosc_porazek+1)
                else:ilosc_porazek+=1
            y[ilosc_porazek+1]=y.get(ilosc_porazek+1,0)+1
        #deklaracja listy self.x=lista ktora ma tyle zer ile wyniosl najpozniejszy sukces
        y2=[0]*(max_sukces+1)
        for i in range(max_sukces+1):#liczy od zera do maxsuk-1
            #zmiana ze slownika na liste
            y2[i]=y.get(i,0)
        return [range(max_sukces+1),y2]
    def generuj_probe_teoretyczna(self,p):
        #zapelnianie wedlug teorii, wartosci zgodne ze wzorem
        y=list()
        y.append(0)
        licznik=0
        q=1.0-p
        ile_sukcesow=int(self.n*p)
        while(ile_sukcesow>=1):
            y.append(ile_sukcesow)
            licznik+=1
            #ile powinno byc sukcesow przy probie n 
            ile_sukcesow=int(self.n*p*(q**licznik))
        return [y,licznik]
class rozkladBernoulliego(rozkladDyskretny):
    # dyskretny rozklad prawdopodobienstwa opisujacy liczbe sukcesow k 
    #w ciagu N niezaleznych prob, z ktorych kazda ma stale 
    #prawdopodobienstwo sukcesu rowne p. 
    def __init__(self,p,n):        
    #p prawdopodobienstwo sukcesu, n ilosc prob
        self.n=n
        z=self.generuj_probe_losowa(p)
        y=z[1]
        x=z[0]
        rozkladDyskretny.__init__(self,p, x, y)
    def generuj_probe_losowa(self, p):
        x=self.n+1
        y=[0]*x
        for i in range(1,x):
            if random()<=p:
                y[i]=1
        return [range(x),y]
    def wariancja(self,p,n):
        return float(n)*p*(1-p)
    def wartosc_oczekiwana(self,p,n):
        return p*n