from rozklady import bernoully
from math import sqrt

p=0.5#prawdopodobienstwo
n=1000000#ilosc prob
#generowanie losowego rozkladu
print "n=",n,"p=",p
losowyBern=bernoully.rozkladBernoulliego(p,n)
ilosc_sukcesow=sum(losowyBern.y)#ilosc sukcesow w n probach
srPr=ilosc_sukcesow/float(len(losowyBern.x)-1)#srednie pr sukcesu dla proby losowej
EX=losowyBern.wartosc_oczekiwana(p,n)
DX=losowyBern.wariancja_teoria(p,n)
sigma=sqrt(DX)
print "sredniasukcesow z proby losowej",losowyBern.EX,"wariancja z proby losowej",losowyBern.DX
print "wartosc oczekiwana",EX,"wariancja wedlug wzoru",losowyBern.wariancja_teoria(p,n),"sigma=",sigma


print "ilosc losowych sukcesow z pr sukcesu",p,"wynosi ",ilosc_sukcesow
print "srednia wartosc prawdopodobienstwa sukcesu z proby losowej(EX dla 1 proby)",srPr


import numpy as np
import matplotlib.pyplot as plt
DX=sqrt(float(DX))
slupki=('EX','EX-DX', 'Zwyciestwa','EX+DX')
x1=(EX,EX-DX,sum(losowyBern.y),EX+DX)


N = len(slupki) # ilosc porazek, sukcesow, EX, srednia


index = np.arange(N)
bar_width = 0.35

rects1 = plt.bar(index, x1, bar_width,
                 alpha=0.4,
                 color='r',
                 label="zxc")
                 
plt.xticks(index + bar_width/2, slupki)
plt.ylabel('Ilosc zwyciestw')
plt.xlabel('Grupy')
plt.tight_layout()
plt.ylim((min(x1)-1, max(x1) ))#minimum
plt.show()
#-----------------------