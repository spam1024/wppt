"""
Metody probabilistyczne algory
Laboratorium 2

Przeprowadzic eksperymenty dla wrzucania kulek do pojemnikow. Wrzucamy m kulek do n pojemnikow, sortujemy pojemniki od najwiekszego do najmniejszego i badamy pojemniki numer 1, 0.1n, 0.5n i 0.9n. Dodatkowo: ile jest pustych pojemnikow. Nalezy przeprowadzic ~1000 eksperymentow i dla kazdego zrobic wykres rozkladu tych czterech/pieciu wartosci. Wartosci n i m sa parametrami, wybrac np. takie:
m = n
m = n log n
m = 10 n log n
dla powyzszych, n: 16, 128, 1024
Eksperymenty przeprowadzic z nastepujacymi warunkami dodatkowymi:
kazda kulka jest wrzucana niezaleznie
kulki sa wrzucane po kolei, kulka wybiera 2 pojemniki i wpada do mniej obciazonego
to samo, co 2, ale kulka wybiera d = log n pojemnikow
losujemy liczbe od 0 do n-1 (tak tez numerujemy pojemniki),  sprawdzamy pojemniki rozniace sie od niej o dokladnie jeden bit i wrzucamy kulke do najmniej obciazonego z nich; przyklad dla n = 128: losujemy 23 = 0010111, co powoduje, ze sprawdzamy pojemniki: 87, 55, 7, 31, 19, 21, 22 (roznia sie od 23 na coraz mniej znaczacych bitach), ale nie 23!


"""
import numpy as np
import pandas as pd
import pickle

def ile_pustych_pojemnikow(probka):
    ile_pustych=1
    while probka[-ile_pustych]==0:
        ile_pustych+=1    
    return ile_pustych-1

def przygotuj_probke(probka,ile_kulek,rodzaj="niezaleznie"):
    ile_pojemnikow=len(probka)    
    if rodzaj=="niezaleznie":
        for i in range(ile_kulek):#zapelniamy urny
            #liczba pojemnika do ktorego wpadla kulka
            x=np.random.randint(0,ile_pojemnikow)#0 do m-1
            probka[x]+=1
    elif rodzaj=="2pojemniki":
         for i in range(ile_kulek):#zapelniamy urny
            #liczba pojemnika do ktorego wpadla kulka
            x1=np.random.randint(0,ile_pojemnikow)#0 do m-1
            x2=np.random.randint(0,ile_pojemnikow)#0 do m-1
            while x1==x2:#losoj dopoki sa takie same
                x2=np.random.randint(0,ile_pojemnikow)#0 do m-1
            if probka[x1]<probka[x2]: x=x1
            else: x=x2
            probka[x]+=1       
    elif rodzaj=="logn-pojemniki":
        for i in range(ile_kulek):#zapelniamy urny
            ile_wybranych_pojemnikow=int(np.log(ile_pojemnikow))
            wybrane_pojemniki=[]
            dostepne_pojemniki=range(ile_pojemnikow)
            #wybiera logn pojemnikow
            while ile_wybranych_pojemnikow>0:
                #wybieramy pojemnik sposrod niewybranych
                x=np.random.randint(0,len(dostepne_pojemniki))
                wybrane_pojemniki.append(dostepne_pojemniki.pop(x))
                ile_wybranych_pojemnikow-=1
            #wrzuca do pojemnika w ktorym jest najmniej z posrod logn wybranych
            min_wartosc=ile_kulek
            min_x=0
            for x in wybrane_pojemniki:
                if probka[x]<min_wartosc: 
                    min_x=x
                    min_wartosc=probka[x]
            probka[min_x]+=1
    elif rodzaj=="roznica-1-bit":
        max_bit=1
        bity=[1]
        while max_bit<=ile_pojemnikow:
            max_bit*=2
            bity.append(max_bit)            

        
        for i in range(ile_kulek):#zapelniamy urny
            #losujemy liczbe od 0 do n-1 (tak tez numerujemy pojemniki)
            x=np.random.randint(0,ile_pojemnikow)#0 do m-1
            min_kulek_pojemnik=ile_kulek
            min_pojemnik=0            
            #sprawdzamy pojemniki rozniace sie od niej o dokladnie jeden bit i 
            
            for b in bity:
                pojemnik_roznica_bit=b^x
                #roznica o bit nie przekracza granicy
                if pojemnik_roznica_bit<ile_pojemnikow:
                    #wybieramy najmniejszy pojemnik
                   if probka[pojemnik_roznica_bit]<min_kulek_pojemnik:
                        min_kulek_pojemnik=probka[pojemnik_roznica_bit]
                        min_pojemnik=pojemnik_roznica_bit
            #wrzucamy kulke do najmniej obciazonego z nich
            probka[min_pojemnik]+=1    
    else:
        probka=[0]
        
        

def rozmiesc_kule_do_pojemnikow(rodzaj="niezaleznie"):  
    n=[16,128,1024] #ilosc pojemnikow
    m=dict()#ilosc kulek
    for i in n:
        logn=np.log(i)
        m[i]=[i, int(i*logn), int(10*i*logn)]
    #0=n, 1=n*logn, 2=10*n*logn
    eksperyment1= dict() #tablice urn z kulkami
    ile_pustych=dict()
    #eksperyment[ilosc pojemnikow][indeks_kulki]
    #eksperyment[n][m] gdzie m wskaznik na ilosc kulek
    #zapelniamy urny
    for pojemnik in n:#ile pojemnikow
        seria_puste=[]#zbior z iloscia pustych pojemnikow
        seria=[]#zbior probek dla danego pojemnika i roznych ilosci kulek
        for kulek in m[pojemnik]:#ile kulek
            probka=[0]*pojemnik#losowa proba dla danego n i m
            przygotuj_probke(probka,kulek,rodzaj)
            #sortujemy pojemniki od najwiekszego do najmniejszego 
            probka.sort(reverse=True)
            seria.append(probka)
            seria_puste.append(ile_pustych_pojemnikow(probka))
        eksperyment1[pojemnik]=seria
        ile_pustych[pojemnik]=seria_puste
    return [eksperyment1,ile_pustych]
    #eksperyment[n][m]-zbior urn z kulkami losowo rozmieszczonymi

    

eksperyment1=[]
eksperyment2=[]
eksperyment3=[]
eksperyment4=[]
eksperyment1.append(rozmiesc_kule_do_pojemnikow())
eksperyment2.append(rozmiesc_kule_do_pojemnikow(rodzaj="2pojemniki"))
eksperyment3.append(rozmiesc_kule_do_pojemnikow(rodzaj="logn-pojemniki"))
eksperyment4.append(rozmiesc_kule_do_pojemnikow(rodzaj="roznica-1-bit"))
#print "posortowane",eksperyment1[0]
print "x1 Jest",eksperyment1[0][1],"pustych pojemnikow"

#2)kulki sa wrzucane po kolei, kulka wybiera 2 pojemniki i wpada do mniej obciazonego
#print "posortowane",eksperyment2[0]
print "x2 Jest",eksperyment2[0][1],"pustych pojemnikow"

#3)to samo, co 2, ale kulka wybiera d = log n pojemnikow
#print "posortowane",eksperyment3[0]
print "x3 Jest",eksperyment3[0][1],"pustych pojemnikow"

#4)losujemy liczbe od 0 do n-1 (tak tez numerujemy pojemniki),  sprawdzamy pojemniki rozniace sie od niej o dokladnie jeden bit i wrzucamy kulke do najmniej obciazonego z nich; przyklad dla n = 128: losujemy 23 = 0010111, co powoduje, ze sprawdzamy pojemniki: 87, 55, 7, 31, 19, 21, 22 (roznia sie od 23 na coraz mniej znaczacych bitach), ale nie 23!
#print "posortowane",eksperyment4[0]
print "x4 Jest",eksperyment4[0][1],"pustych pojemnikow"
#-------------------------------------------------------------------
#badamy pojemniki numer 1, 0.1n, 0.5n i 0.9n. 
#Dodatkowo: ile jest pustych pojemnikow.
# Nalezy przeprowadzic ~1000 eksperymentow
for i in range(1000):
    print "obliczanie eksperymentow",i*100/1000.0,"%"
    eksperyment1.append(rozmiesc_kule_do_pojemnikow())
    eksperyment2.append(rozmiesc_kule_do_pojemnikow(rodzaj="2pojemniki"))
    eksperyment3.append(rozmiesc_kule_do_pojemnikow(rodzaj="logn-pojemniki"))
    eksperyment4.append(rozmiesc_kule_do_pojemnikow(rodzaj="roznica-1-bit"))
# i dla kazdego zrobic wykres rozkladu tych czterech/pieciu wartosci

#-------------------------------------------------------------
print "zapisz wyniku do pliku"
#zapisz wynik do pliku
eksperyment=[eksperyment1,eksperyment2,eksperyment3,eksperyment4]
plik_wynikowy=open("lab2","w")
pickle.dump(eksperyment,plik_wynikowy)
plik_wynikowy.close()
del eksperyment1, eksperyment2, eksperyment3,eksperyment4,eksperyment,i
print "koniec"
"""
1.kazda kulka jest wrzucana niezaleznie
2.kulki sa wrzucane po kolei, kulka wybiera 2 pojemniki i wpada do mniej obciazonego
3.to samo, co 2, ale kulka wybiera d = log n pojemnikow
4.losujemy liczbe od 0 do n-1 (tak tez numerujemy pojemniki),  
#sprawdzamy pojemniki rozniace sie od niej o dokladnie jeden bit i 
#wrzucamy kulke do najmniej obciazonego z nich; 
#przyklad dla n = 128: losujemy 23 = 0010111, co powoduje,
# ze sprawdzamy pojemniki: 87, 55, 7, 31, 19, 21, 22 (roznia sie od 23 na coraz mniej
 znaczacych bitach), ale nie 23!
"""
