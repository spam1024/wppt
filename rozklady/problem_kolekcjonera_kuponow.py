import geometryczny
"""
Przypuscmy, ze kazdego dnia pudelko platkow sniadaniowych zawiera jeden sposrod n roznych kuponow.
Kiedy zbierzemy po jednym kuponie kazdego typu, to mozemy je odeslac, by otrzymac nagrode.
Zakladaja, ze kupony sa przypisane pudelkom w sposob niezalezny z jednakowym prawdopodobienstwem na zbiorze n mozliwosci.
Ile pudelek platkow nalezy kupic, aby otrzymac przynajmniej po 1 kuponie kazdego rodzaju?
"""
class kolekcjonerKuponow:
    def __init__(self,ilosc_kuponow):#n-ilosc kuponow
        self.ilosc_kuponow= ilosc_kuponow
        #prawdopodobienstwo otrzymania kuponu nowego rodzaju po zebraniu i-1 roznych rodzajow
        self.pr_kupon=self.oblicz_prawdopodobienstwo()        
        # dla pudelka[i] ilosc pudelek od poprzedniego i-1 wylosowania kuponu nowego rodzaju
        self.pudelka=self.generuj_probe_losowa()
        #
        self.ilosc_pudelek=self.oblicz_pudelka()
        #self.EX=self.srednia(self)
        #self.DX=self.wariancja(self)
    def oblicz_prawdopodobienstwo(self):
        #prawdopodobienstwo otrzymania kuponu nowego rodzaju po zebraniu i-1 roznych rodzajow
        return tuple([0]+[1-(i-1)/float(self.ilosc_kuponow) for i in range(1,self.ilosc_kuponow+1) ])
    def generuj_probe_losowa(self):
        # dla pudelka[i] ilosc pudelek od poprzedniego i-1 wylosowania kuponu nowego rodzaju
        z=[0]
        geo=geometryczny.rozkladGeometryczny()
        for i in range(1,self.ilosc_kuponow+1):
            z.append(geo.generuj_probe_losowa(self.pr_kupon[i]) )
            print z,i,"tak\n" 
        print "\n\n\n\n\n2... z to ",z,"\n\n\n\n\n"
        return tuple([0]+[z[i][1] for i in range(1,self.ilosc_kuponow+1)])
    def srednia(self):
        pass
    def wariancja(self):
        pass
    def wartosc_oczekiwana(self,ile_kuponow):
        return sum([ ile_kuponow/float(ile_kuponow-i+1) for i in range(1,ile_kuponow+1)])
    def oblicz_pudelka(self):
        pass#return sum(self.pudelka) 