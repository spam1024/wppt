
from random import random
from dyskretny import *

class rozkladGeometryczny(rozkladDyskretny):
    #rozklad geometryczny  dyskretny rozklad prawdopodobienstwa 
    #opisujacy prawdopodobienstwo zdarzenia, ze proces Bernoulliego odniesie 
    #pierwszy sukces dokladnie w k tej probie    
    def __init__(self,p=0.5,n=1,losowa=1):
        #p-prawdopodobienstwo,n-ilosc prob, jesli klasa ma byc losowa=1
        self.n=n#ilosc prob
        if losowa==1:
            #zwraca tablice x oraz w ktorej probie najpozniejszy sukces
            z=self.generuj_probe_losowa(p) 
            x=z[0]
            y=z[1]
        else:
            z=list()
            z=self.generuj_probe_teoretyczna(p)    
            y=z[1]
            x=z[0]#sprawdza jeden do przodu nie trzeba dodawac+1
        rozkladDyskretny.__init__(self, p, x, y)
        self.EX=self.srednia()
        self.DX=self.wariancja_populacji()
    def generuj_probe_losowa(self,p):
        #zapelnianie wedlug wygenerowanych liczb np sukces za 3razem [0 piersze musibyc bo od 0 licznik]0,0,1
        max_sukces=0#nawieksza liczba proby dla ktorej wylosowano zwyciestwo
        y=dict()        
        for i in range(self.n):
            #przeprowadzamy n prob
            sukces=0
            ilosc_porazek=0
            while(sukces==0):
                probka=random()
                if(probka<p):
                    sukces=1
                    max_sukces=max(max_sukces,ilosc_porazek+1)
                else:ilosc_porazek+=1
            y[max_sukces+1]=y.get(max_sukces+1,0)+1
        #deklaracja listy self.x=lista ktora ma tyle zer ile wyniosl najpozniejszy sukces
        y2=[0]*(max_sukces+1)
        for i in range(max_sukces+1):#liczy od zera do maxsuk-1
            #zmiana ze slownika na liste
            y2[i]=y.get(i,0)
        print "\ny2 tp ",y2,max_sukces
        return [range(max_sukces+1),y2]
    def generuj_probe_teoretyczna(self,p):
        #zapelnianie wedlug teorii, wartosci zgodne ze wzorem
        y=list()
        y.append(0)
        licznik=0
        q=1.0-p
        ile_sukcesow=int(self.n*p)
        while(ile_sukcesow>=1):
            y.append(ile_sukcesow)
            licznik+=1
            #ile powinno byc sukcesow przy probie n 
            ile_sukcesow=int(self.n*p*(q**licznik))
        return [y,licznik]
    def wartosc_oczekiwana(self,p):
        return 1/p
    def wariancja(self,p):
        return (1-p)/float(p**2)
    def srednia(self):#srednia za ktorym razem wygra o
        srednia=0
        for i in range(1,len(self.x)):#pierwsza wartosc pusta
            srednia+=i*self.y[i]
        return (srednia/float(self.n)) 
    def wariancja_populacji(self):
        wariancja=0
        for i in range(1,len(self.x)):
            wariancja=(self.EX-self.y[i])**2
        return wariancja