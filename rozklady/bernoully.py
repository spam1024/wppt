from random import random
from dyskretny import *


class rozkladBernoulliego(rozkladDyskretny):
    # dyskretny rozklad prawdopodobienstwa opisujacy liczbe sukcesow k 
    #w ciagu N niezaleznych prob, z ktorych kazda ma stale 
    #prawdopodobienstwo sukcesu rowne p. 
    def __init__(self,p,n):        
    #p prawdopodobienstwo sukcesu, n ilosc prob
        self.n=n
        z=self.generuj_probe_losowa(p)
        y=z[1]
        x=z[0]
        #liczymy wariancje
        rozkladDyskretny.__init__(self,p, x, y)
        self.EX=sum(self.y)
        self.DX=self.wariancja_populacji()
    def generuj_probe_losowa(self, p):
        x=self.n+1
        y=[0]*x
        for i in range(1,x):
            if random()<=p:
                y[i]=1
        return [range(x),y]
    def wariancja_teoria(self,p,n):
        return float(n)*p*(1-p)
    def wariancja_populacji(self):
        wariancja=0
        EXproba=sum(self.y)/float(self.n)#wszystkie zwyciestwa / ilosc prob
        for i in self.x:
            wariancja+=(EXproba-self.y[i])**2
        wariancja=self.n*wariancja/float(len(self.x))#w 0 pusta wartosc
        return wariancja
    def wartosc_oczekiwana(self,p,n):
        return p*n