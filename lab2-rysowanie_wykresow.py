# -*- coding: utf-8 -*-

import pickle
import matplotlib.pyplot as plt
import numpy as np

#----------------start---------------
plik_wejsciowy=open("lab2","r")
ile_urn=-1
ile_kulek=-1
#------------------------------------
while (ile_urn!=16)and(ile_urn!=128)and(ile_urn!=1024):
    ile_urn=int(raw_input("Podaj ilosc kulek do wyboru n: 16, 128, 1024\n"))

while(ile_kulek!=3)and(ile_kulek!=1)and(ile_kulek!=2):
    ile_kulek=int(raw_input("Wybierz ile kulek do wyboru\n1.m = n\n2.m = n log n\n3.m = 10 n logn\ngdzie n to ilosc urn\n"))
print "jest",ile_urn,"urn i opcja",ile_kulek,"kulek"
ile_kulek-=1
eksperyment_wszystko=pickle.load(plik_wejsciowy)
#nr eksperymentu[0-3],
#nr probki[0-1000]
#eksp czy ile_pustych[0-1]
#ile urn[16,128,1024]
#ktora probka m = n---m = n log n---m = 10 n log n---[0-2]
#eksperyment= eksperyment_wszystko[nr_eksperymentu][0][0][ile_urn][ile_kulek]
urny=[0,int(ile_urn/10.0), int(ile_urn/2.0),int(9*ile_urn/10.0)]
wyniki=[]
for nr_eksperymentu in range(4):
    eksperymentx=[0]*5
    for nr_probki in range(1000):
        try:
            eksperymentx[0]+=eksperyment_wszystko[nr_eksperymentu][nr_probki][0][ile_urn][ile_kulek][urny[0]]
            eksperymentx[1]+=eksperyment_wszystko[nr_eksperymentu][nr_probki][0][ile_urn][ile_kulek][urny[1]]
            eksperymentx[2]+=eksperyment_wszystko[nr_eksperymentu][nr_probki][0][ile_urn][ile_kulek][urny[2]]
            eksperymentx[3]+=eksperyment_wszystko[nr_eksperymentu][nr_probki][0][ile_urn][ile_kulek][urny[3]]
            eksperymentx[4]+=eksperyment_wszystko[nr_eksperymentu][nr_probki][1][ile_urn][ile_kulek]
        except IndexError:
            print "nr_eksperymentu",nr_eksperymentu,"nr_probki",nr_probki,"ile_urn",ile_urn,"ile_kulek",ile_kulek
            break
    for ix in range(5):
        eksperymentx[ix]/=1000.0
    wyniki.append(eksperymentx)
#urny=[]
#urny.append()
#for i in urny:
#    wyniki.append(eksp[i])
#dla 4 eksperymentow
plt.ylabel('Ilosc kul w pojemniku')
plt.xlabel('dla pojemnika 1=1, 2=0.1n, 3=0.5n, 4=0.9n, 5=ilosc pustych')
#kazda kulka jest wrzucana niezaleznie
plt.plot([1,2,3,4,5],wyniki[0],"b-")
#kulki sa wrzucane po kolei, kulka wybiera 2 pojemniki i wpada do mniej obciazonego
plt.plot([1,2,3,4,5],wyniki[1],"r--")
#to samo, co 2, ale kulka wybiera d = log n pojemnikow
plt.plot([1,2,3,4,5],wyniki[2],"-.")
#losujemy liczbe od 0 do n-1 (tak tez numerujemy pojemniki), 
plt.plot([1,2,3,4,5],wyniki[3],"g:")
plt.show()

#plt.plot([1,2,3,4],wyniki4)
#czyszczenie pamieci
del eksperyment_wszystko,eksperymentx,ile_kulek,ile_urn,ix,nr_eksperymentu,nr_probki,urny,wyniki
